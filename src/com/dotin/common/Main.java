package com.dotin.common;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

import com.dotin.entity.Deposit.depositType;

public class Main {

    public static void main(String[] arg) {
        List<Object> objectList = readXml();
        compare(objectList);
        writeToFile(objectList);


    }

    private static void writeToFile(List<Object> objectList) {
        File file = new File("result.txt");
        try {
            BufferedWriter output = new BufferedWriter(new FileWriter(file));
            for (Object object : objectList) {
                Object number = invokeGetter(object, "customerNumber");
                Object interest = invokeGetter(object, "Interest");
                output.write(number.toString());
                output.write("#");
                output.write(interest.toString());
                output.write("\n");


            }
            output.close();
        } catch (Exception e) {
            e.getMessage();
        }

    }


    private static void compare(List<Object> objectList) {
        final String attribute = "Interest"; // for example. Also, this is case-sensitive
        Collections.sort(objectList, new Comparator<Object>() {
            public int compare(Object o1, Object o2) {
                try {
                    Method m = o1.getClass().getMethod("get" + attribute);
                    // Assume String type. If different, you must handle each type
                    Double s1 = (Double) m.invoke(o1);
                    Double s2 = (Double) m.invoke(o2);
                    return s2.compareTo(s1);
                    // simply re-throw checked exceptions wrapped in an unchecked exception
                } catch (SecurityException e) {
                    throw new RuntimeException(e);
                } catch (NoSuchMethodException e) {
                    throw new RuntimeException(e);
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                } catch (InvocationTargetException e) {
                    throw new RuntimeException(e);
                }
            }
        });

    }

    private static List<Object> readXml() {
        List<Object> objects = new ArrayList<>();
        String customerNumber = null;
        String depositType1 = null;
        Double depositBalance = null;
        int durationDays = 0;
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(new File("file.xml"));
            document.getDocumentElement().normalize();
            NodeList nodeList = document.getElementsByTagName("deposit");
            int temp = 0;
            while (temp < nodeList.getLength()) {
                Node node = nodeList.item(temp);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    customerNumber = element.getElementsByTagName("customerNumber").item(0).getTextContent();
                    depositType1 = element.getElementsByTagName("depositType").item(0).getTextContent();
                    depositBalance = Double.parseDouble(element.getElementsByTagName("depositBalance").item(0).getTextContent());
                    durationDays = Integer.parseInt(element.getElementsByTagName("durationDays").item(0).getTextContent());
                }

                boolean valid = validate(depositBalance, depositType1, durationDays);
                if (valid == true) {
                    Class<?> clazz = Class.forName("com.dotin.entity." + depositType1);
                    Object instance = clazz.newInstance();
                    invokeSetter(instance, "customerNumber", customerNumber);
                    invokeSetter(instance, "depositBalance", depositBalance);
                    invokeSetter(instance, "durationDays", durationDays);
                    Method method = instance.getClass().getMethod("PayedInterest");
                    method.invoke(instance);
                    objects.add(instance);

                    temp++;

                } else
                    temp++;


            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return objects;
    }


    private static void invokeSetter(Object instance, String propertyName, Object variableValue) {
        PropertyDescriptor pd;
        try {
            pd = new PropertyDescriptor(propertyName, instance.getClass());
            Method setter = pd.getWriteMethod();
            try {
                setter.invoke(instance, variableValue);
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                e.printStackTrace();
            }
        } catch (IntrospectionException e) {
            e.printStackTrace();
        }
    }

    private static boolean set(Object instance, String fieldName, Object fieldValue) {
        Class<?> clazz = instance.getClass();
        while (clazz != null) {
            try {
                Field field = clazz.getDeclaredField(fieldName);
                field.setAccessible(true);
                field.set(instance, fieldValue);
                return true;
            } catch (NoSuchFieldException e) {
                clazz = clazz.getSuperclass();
            } catch (Exception e) {
                throw new IllegalStateException(e);
            }
        }
        return false;
    }

    private static boolean validate(Double depositBlance, String depositType1, int durationDay) {
        boolean valid = true;
        try {
            if (depositBlance < 0) {
                valid = false;
                throw new Exception("depositBalance not valid");

            }
            if (durationDay <= 0) {
                valid = false;
                throw new Exception("durationDays not valid");

            }
            if (!checkDepositType(depositType1)) {
                valid = false;
                throw new Exception("depositType not valid");
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return valid;
        }
    }

    private static boolean checkDepositType(String depositType1) {
        for (depositType me : depositType.values()) {
            if (me.name().equalsIgnoreCase(depositType1))
                return true;
        }
        return false;
    }

    public static Object invokeGetter(Object obj, String variableName) {
        Object f = null;
        try {
            PropertyDescriptor pd = new PropertyDescriptor(variableName, obj.getClass());
            Method getter = pd.getReadMethod();
            f = getter.invoke(obj);


        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | IntrospectionException e) {
            e.printStackTrace();
        }
        return f;
    }
}


