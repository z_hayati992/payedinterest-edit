package com.dotin.entity;

public class Deposit {
    private String customerNumber;

    public enum depositType {
        LongTerm(2),
        ShortTerm(1),
        Gharz(0);
        private int value;

        private depositType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    private Double depositBalance;
    private int durationDays;
    private Double Interest;

    public int getIntersest_Rate() {
        return intersest_Rate;
    }

    protected int intersest_Rate;

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }


    public Double getDepositBalance() {
        return depositBalance;
    }

    public void setDepositBalance(Double depositBalance) {
        this.depositBalance = depositBalance;
    }

    public int getDurationDays() {
        return durationDays;
    }

    public void setDurationDays(int durationDays) {
        this.durationDays = durationDays;
    }

    public Double getInterest() {
        return Interest;
    }

    public void setInterest(Double interest) {
        Interest = interest;
    }

    public void PayedInterest() {

        double result = ((getDepositBalance() * getDurationDays() * getIntersest_Rate()) / 36500);
        setInterest(result);
    }


}
